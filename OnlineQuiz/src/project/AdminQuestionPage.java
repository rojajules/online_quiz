package project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import java.awt.Color;

public class AdminQuestionPage extends JFrame{
	private JTextField txtAddQuestions;
	private JTextField questiontextField;
	private JTextField textFieldA;
	private JTextField textFieldB;
	private JTextField textFieldC;
	private JTextField textFieldD;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel labelA;
	private JLabel labelB;
	private JLabel labelC;
	private JLabel labelD;
	private JTextField qtextfield;
	public AdminQuestionPage() {
		getContentPane().setBackground(Color.CYAN);
		getContentPane().setLayout(null);
		
		txtAddQuestions = new JTextField();
		txtAddQuestions.setFont(new Font("Tahoma", Font.BOLD, 10));
		txtAddQuestions.setText("ADD QUESTIONS");
		txtAddQuestions.setBounds(10, 26, 117, 19);
		getContentPane().add(txtAddQuestions);
		txtAddQuestions.setColumns(10);
		
		questiontextField = new JTextField();
		questiontextField.setFont(new Font("Tahoma", Font.BOLD, 16));
		questiontextField.setBounds(63, 68, 589, 36);
		getContentPane().add(questiontextField);
		questiontextField.setColumns(10);
		
		textFieldA = new JTextField();
		textFieldA.setFont(new Font("Tahoma", Font.BOLD, 16));
		textFieldA.setColumns(10);
		textFieldA.setBounds(63, 114, 589, 36);
		getContentPane().add(textFieldA);
		
		textFieldB = new JTextField();
		textFieldB.setFont(new Font("Tahoma", Font.BOLD, 16));
		textFieldB.setColumns(10);
		textFieldB.setBounds(63, 160, 589, 36);
		getContentPane().add(textFieldB);
		
		textFieldC = new JTextField();
		textFieldC.setFont(new Font("Tahoma", Font.BOLD, 16));
		textFieldC.setColumns(10);
		textFieldC.setBounds(63, 206, 589, 36);
		getContentPane().add(textFieldC);
		
		textFieldD = new JTextField();
		textFieldD.setFont(new Font("Tahoma", Font.BOLD, 16));
		textFieldD.setColumns(10);
		textFieldD.setBounds(63, 267, 589, 36);
		getContentPane().add(textFieldD);
		
		JRadioButton Aradio = new JRadioButton("A");
		buttonGroup.add(Aradio);
		Aradio.setFont(new Font("Tahoma", Font.BOLD, 10));
		Aradio.setBounds(194, 378, 39, 21);
		getContentPane().add(Aradio);
		
		JRadioButton radioB = new JRadioButton("B");
		buttonGroup.add(radioB);
		radioB.setFont(new Font("Tahoma", Font.BOLD, 10));
		radioB.setBounds(245, 378, 39, 21);
		getContentPane().add(radioB);
		
		JRadioButton radioC = new JRadioButton("C");
		buttonGroup.add(radioC);
		radioC.setFont(new Font("Tahoma", Font.BOLD, 10));
		radioC.setBounds(300, 378, 39, 21);
		getContentPane().add(radioC);
		
		JRadioButton radioD = new JRadioButton("D");
		buttonGroup.add(radioD);
		radioD.setFont(new Font("Tahoma", Font.BOLD, 10));
		radioD.setBounds(356, 378, 39, 21);
		getContentPane().add(radioD);
		
		JLabel lblNewLabel = new JLabel("CORRECT ANSWER");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 10));
		lblNewLabel.setBounds(10, 382, 154, 21);
		getContentPane().add(lblNewLabel);
		
		labelA = new JLabel("A");
		labelA.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelA.setBounds(10, 117, 39, 29);
		getContentPane().add(labelA);
		
		labelB = new JLabel("B");
		labelB.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelB.setBounds(10, 160, 39, 29);
		getContentPane().add(labelB);
		
		labelC = new JLabel("C");
		labelC.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelC.setBounds(10, 206, 39, 29);
		getContentPane().add(labelC);
		
		labelD = new JLabel("D");
		labelD.setFont(new Font("Tahoma", Font.BOLD, 17));
		labelD.setBounds(10, 267, 39, 29);
		getContentPane().add(labelD);
		
		qtextfield = new JTextField();
		qtextfield.setFont(new Font("Tahoma", Font.BOLD, 17));
		qtextfield.setBounds(7, 72, 46, 32);
		getContentPane().add(qtextfield);
		qtextfield.setColumns(10);
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==btnAdd) {
					int question_no;
					String question_name;
					String answer_1,answer_2,answer_3,answer_4;
					String correct_answer = null;
					question_no=Integer.parseInt(qtextfield.getText());
					question_name=questiontextField.getText();
					answer_1=textFieldA.getText();
					answer_2=textFieldB.getText();
					answer_3=textFieldC.getText();
					answer_4=textFieldD.getText();
					if(Aradio.isSelected()) 
						correct_answer=answer_1;
					if(radioB.isSelected())
						correct_answer=answer_2;
					if(radioC.isSelected())
						correct_answer=answer_3;
					if(radioD.isSelected())
						correct_answer=answer_4;

					SqlAdminQuestion s1=new SqlAdminQuestion();
					s1.insertRecord( question_no, question_name, answer_1, answer_2, answer_3, answer_4, correct_answer); 

					}
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAdd.setBounds(489, 367, 124, 36);
		getContentPane().add(btnAdd);
		
		JButton btnLogout = new JButton("LOGOUT");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==btnLogout) {
					firstpage a1=new firstpage();
					a1.setVisible(true);
					 a1.setBounds(50, 50, 600,600);
				}
			}
		});
		btnLogout.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnLogout.setBounds(675, 367, 124, 36);
		getContentPane().add(btnLogout);
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AdminQuestionPage aqp=new AdminQuestionPage();
		aqp.setTitle("Admin QuestionForm");
		aqp.setVisible(true);
        aqp.setBounds(10, 10, 370, 600);
        aqp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       aqp.setResizable(false);

	}
}