package project;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.Color;


public class UserSignupPage extends JFrame implements ActionListener{
	Container container = getContentPane();
    JLabel userLabel = new JLabel("USERNAME");
    JLabel passwordLabel = new JLabel("PASSWORD");
    JTextField userTextField = new JTextField();
    JPasswordField passwordField = new JPasswordField();
    JButton btnok = new JButton("Signup");
    private JTextField mobiletextField;
    private JTextField emailTextField;
    private JPasswordField confrompasswordField;


    UserSignupPage() {
    	getContentPane().setBackground(Color.CYAN);
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();

    }

    public void setLayoutManager() {
        container.setLayout(null);
    }

    public void setLocationAndSize() {
        userLabel.setBounds(50, 82, 100, 30);
        passwordLabel.setBounds(50, 232, 100, 30);
        userTextField.setBounds(198, 83, 150, 30);
        passwordField.setBounds(198, 233, 150, 30);
        btnok.setBounds(182, 355, 100, 30);


    }

    public void addComponentsToContainer() {
        container.add(userLabel);
        container.add(passwordLabel);
        container.add(userTextField);
        container.add(passwordField);
        container.add(btnok);
        
        JLabel mobileLabel = new JLabel("MOBILE NO");
        mobileLabel.setBounds(50, 122, 100, 30);
        getContentPane().add(mobileLabel);
        
        mobiletextField = new JTextField();
        mobiletextField.setBounds(198, 123, 150, 30);
        getContentPane().add(mobiletextField);
        
        JLabel emailLabel = new JLabel("EMAIL");
        emailLabel.setBounds(50, 173, 100, 30);
        getContentPane().add(emailLabel);
        
        emailTextField = new JTextField();
        emailTextField.setBounds(198, 174, 150, 30);
        getContentPane().add(emailTextField);
        
        JLabel lblConfromPassword = new JLabel("CONFROM PASSWORD");
        lblConfromPassword.setBounds(50, 290, 121, 30);
        getContentPane().add(lblConfromPassword);
        
        confrompasswordField = new JPasswordField();
        confrompasswordField.setBounds(198, 291, 150, 30);
        getContentPane().add(confrompasswordField);
    }

    public void addActionEvent() {
        btnok.addActionListener(this);
    }


   
    

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserSignupPage frame = new UserSignupPage ();
         frame.setTitle("signup form");
         frame.setVisible(true);
         frame.setBounds(10, 10, 370, 600);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.setResizable(false);

	}

	public void actionPerformed(ActionEvent e) {
		 if (e.getSource() == btnok) {
			 System.out.println("done");
			// int user_id;
	            String user_name;
	            String password;
	            String email;
	            int mobilenumber;
	            String confrompassword;
	            //user_id=Integer.parseInt(useridtextField.getText());
	            user_name = userTextField.getText();
	            email=emailTextField.getText();
	            mobilenumber= Integer.parseInt(mobiletextField.getText());
	            password = passwordField.getText();
	            confrompassword=confrompasswordField.getText();
	         
	           
	            SqlSignup s = new SqlSignup();
	           
	            s.insertRecord(user_name,mobilenumber,email,password);
	          
	            Userloginpage u1=new Userloginpage();
	          
	            u1.setVisible(true);
	            u1.setBounds(50, 50, 600,600);
	            
	            
	}
   }
}
