package project;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.Color;


public class Userloginpage extends JFrame implements ActionListener {

	Container container = getContentPane();
    JLabel userLabel = new JLabel("USERNAME");
    JLabel passwordLabel = new JLabel("PASSWORD");
    JTextField userTextField = new JTextField();
    JPasswordField passwordField = new JPasswordField();
    JButton loginButton = new JButton("LOGIN");
    JButton signupbutton = new JButton("SIGNUP");
    JCheckBox showPassword = new JCheckBox("Show Password");


    Userloginpage() {
    	getContentPane().setBackground(Color.CYAN);
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();

    }

    public void setLayoutManager() {
        container.setLayout(null);
    }

    public void setLocationAndSize() {
        userLabel.setBounds(50, 150, 100, 30);
        passwordLabel.setBounds(50, 220, 100, 30);
        userTextField.setBounds(150, 150, 150, 30);
        passwordField.setBounds(150, 220, 150, 30);
        showPassword.setBounds(150, 250, 150, 30);
        loginButton.setBounds(50, 300, 100, 30);
        signupbutton.setBounds(200, 300, 100, 30);


    }

    public void addComponentsToContainer() {
        container.add(userLabel);
        container.add(passwordLabel);
        container.add(userTextField);
        container.add(passwordField);
        container.add(showPassword);
        container.add(loginButton);
        container.add(signupbutton);
    }

    public void addActionEvent() {
        loginButton.addActionListener(this);
        signupbutton.addActionListener(this);
        showPassword.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String user_name;
            String password;
            user_name = userTextField.getText();
            password = passwordField.getText();
            SqlUserPage s=new SqlUserPage();
            if(s.logincheck(user_name,password))
   		 {
   			 JOptionPane.showMessageDialog(this, "Password is correct");
   			TakeTest t1=new TakeTest();
   			t1.setVisible(true);
   			t1.setVisible(true);
			 t1.setBounds(50, 50, 600,600);
 
   		
   		 }
   		 else {
   			 JOptionPane.showMessageDialog(this, "Password is incorrect");
   		 }
        }
        if (e.getSource() == signupbutton) {
            //userTextField.setText("");
           // passwordField.setText("");
        	UserSignupPage u1=new UserSignupPage();
        	u1.setVisible(true);
        	 u1.setBounds(50, 50, 600,600);
        }
        if (e.getSource() == showPassword) {
            if (showPassword.isSelected()) {
                passwordField.setEchoChar((char) 0);
            } else {
                passwordField.setEchoChar('*');
            }


        }
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Userloginpage frame = new Userloginpage ();
        frame.setTitle("Login Form");
        frame.setVisible(true);
        frame.setBounds(10, 10, 370, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);


	}

}
