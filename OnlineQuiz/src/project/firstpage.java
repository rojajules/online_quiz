package project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;

public class firstpage extends JFrame  {
	public firstpage() {
		getContentPane().setBackground(Color.CYAN);
		getContentPane().setLayout(null);
		
		JButton btnAdmin = new JButton("Admin");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == btnAdmin){
					AdminLoginPage a=new AdminLoginPage();
					a.setVisible(true);
					 a.setBounds(50, 50, 600,600);
				}
			}
		});
		btnAdmin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAdmin.setBounds(289, 187, 140, 36);
		getContentPane().add(btnAdmin);
		
		JButton btnUser = new JButton("User");
		btnUser.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == btnUser){
					Userloginpage u1=new Userloginpage();
					u1.setVisible(true);
					 u1.setBounds(50, 50, 600,600);
					 //firstpage().setVisible(false);
				}
			}
		});
		btnUser.setBounds(289, 283, 140, 36);
		getContentPane().add(btnUser);
		
		JLabel lblNewLabel = new JLabel("QUIZ MANAGEMENT");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 32));
		lblNewLabel.setBounds(195, 59, 364, 44);
		getContentPane().add(lblNewLabel);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		firstpage f1=new firstpage();
		f1.setTitle("first page");
		 f1.setVisible(true);
		 f1.setBounds(10, 10, 700, 700);
		  f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f1.setResizable(false);

	}
}
