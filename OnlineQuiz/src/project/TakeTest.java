package project;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
public class TakeTest extends JFrame {
	private JTextField txtInstructions;
	public TakeTest() {
		getContentPane().setBackground(Color.CYAN);
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().setLayout(null);
		
		JButton TakeTestButton = new JButton("TakeTest");
		TakeTestButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TestPage t1= new TestPage();
				t1.setVisible(true);
				 t1.setBounds(50, 50, 600,600);
			}
		});
		TakeTestButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		TakeTestButton.setBounds(219, 313, 147, 27);
		getContentPane().add(TakeTestButton);
		
		txtInstructions = new JTextField();
		txtInstructions.setFont(new Font("Tahoma", Font.BOLD, 18));
		txtInstructions.setText("INSTRUCTIONS");
		txtInstructions.setBounds(10, 44, 356, 27);
		getContentPane().add(txtInstructions);
		txtInstructions.setColumns(10);
		
		JTextArea txtrEachQuestionIn = new JTextArea();
		txtrEachQuestionIn.setText("Each question in the quiz is of multiple-choice or \"true or false\" format.");
		txtrEachQuestionIn.setFont(new Font("Monospaced", Font.BOLD, 18));
		txtrEachQuestionIn.setBounds(10, 91, 738, 27);
		getContentPane().add(txtrEachQuestionIn);
		
		JTextArea txtrReadEachQuestion = new JTextArea();
		txtrReadEachQuestion.setFont(new Font("Monospaced", Font.BOLD, 18));
		txtrReadEachQuestion.setText("Read each question carefully, and answer it carefully.");
		txtrReadEachQuestion.setBounds(10, 137, 738, 27);
		getContentPane().add(txtrReadEachQuestion);
		
		JTextArea txtrEachQuestionCarries = new JTextArea();
		txtrEachQuestionCarries.setText("Each question carries one mark.");
		txtrEachQuestionCarries.setFont(new Font("Monospaced", Font.BOLD, 18));
		txtrEachQuestionCarries.setBounds(10, 174, 738, 27);
		getContentPane().add(txtrEachQuestionCarries);
		
		JTextArea txtrNoNegativeMarks = new JTextArea();
		txtrNoNegativeMarks.setText("No negative marks.");
		txtrNoNegativeMarks.setFont(new Font("Monospaced", Font.BOLD, 18));
		txtrNoNegativeMarks.setBounds(10, 211, 738, 27);
		getContentPane().add(txtrNoNegativeMarks);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TakeTest test = new TakeTest();
		test.setTitle("Take Test");
        test.setVisible(true);
        test.setBounds(10, 10, 700, 700);
        test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        test.setResizable(false);


	}

}
